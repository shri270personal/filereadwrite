package com.example.scrollingactivity1;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.scrollingactivity1.FileReadTest.FileReadViewModel;
import com.example.scrollingactivity1.FileReadTest.FileRepository;


public class TextFileReaderActivity extends AppCompatActivity     {





   private Button dummyButton;
    private Button dummyButton2;
    private Button dummyButton3;
    Context mContext ;

    int PICKFILE_RESULT_CODE = 1221;

    private TextView mTextView ;
    private TextView wrong_email;
private android.widget.EditText editText;

    private FileReadViewModel  mViewModel;

    TextFileReaderActivity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  binding = ActivityExcelFileReaderBinding.inflate(getLayoutInflater());
        setContentView(R.layout.activity_excel_file_reader);
        mContext = this;

        mTextView = (TextView)findViewById(R.id.fullscreen_content);
        wrong_email = (TextView)findViewById(R.id.wrong_email);
        dummyButton = (Button)findViewById(R.id.dummy_button);
        dummyButton2 = (Button)findViewById(R.id.dummy_button2);
        editText = (EditText)findViewById(R.id.editText);
        mActivity = this;
        dummyButton3 = (Button)findViewById(R.id.dummy_button3);
        dummyButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editText.getText().toString().equals("")) {
                    FileRepository.getInstance(mContext).savePublicly(mActivity, mViewModel, editText.getText().toString());

                }
                else {
                    Toast.makeText(mContext, "Enter some text",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        mViewModel = new ViewModelProvider(this).get(FileReadViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.getalldata().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!= null)
                    mTextView.setText(s);
                else mTextView.setText("String is null");
            }
        });

        mViewModel.getfileData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!= null)
                    wrong_email.setText(s);
                else wrong_email.setText("file is null");
            }
        });
    }








}