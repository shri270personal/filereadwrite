package com.example.scrollingactivity1;

import android.os.Bundle;

import com.example.scrollingactivity1.ui.main.ApiClient;
import com.example.scrollingactivity1.ui.main.GitHubService;
import com.example.scrollingactivity1.ui.main.ProjectRepository;
import com.example.scrollingactivity1.ui.main.StoriesList;
import com.example.scrollingactivity1.ui.main.StoryName;
import com.example.scrollingactivity1.ui.main.TestViewModel;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scrollingactivity1.databinding.ActivityScrollingBinding;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrollingActivity extends AppCompatActivity {

    private ActivityScrollingBinding binding;
    private TextView mTextView;
    private TestViewModel mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityScrollingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = binding.toolbarLayout;
        toolBarLayout.setTitle(getTitle());



        mViewModel = new ViewModelProvider(this).get(TestViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.getMessageContainerA().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
if(s!= null)
                mTextView.setText(s);
else mTextView.setText("String is null");
            }
        });

        ProjectRepository.getInstance().loadDataOnViewHolder(getApplicationContext(), mViewModel);
/*
        GitHubService mGithubService = ApiClient.getClient().create(GitHubService.class);
        Call<StoriesList>  call=  mGithubService.doGetStoriesList();
        call.enqueue(new Callback<StoriesList>() {
            @Override
            public void onResponse(Call<StoriesList> call, Response<StoriesList> response) {
                StoriesList mList = response.body();

                List<StoryName> list = mList.getList();

                StringBuilder mStringb = new StringBuilder();
                for(int i =0 ; i<list.size();i++)
                {
                    if(list.get(i).getType().equals("items"))
                    {
                        mStringb.append(list.get(i).getSalesItemList().get(0).getItemName()+",");
                    }
                }
                Log.d("TAGRAM", "Number of items received: " + mStringb);
                Toast.makeText(getApplicationContext(), mList.getResult(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<StoriesList> call, Throwable t) {
                Log.d("TAGRAMFAIL", "Number of movies received: " + t.toString());
                Toast.makeText(getApplicationContext(),"FAIL", Toast.LENGTH_SHORT).show();
            }
        });


*/

        FloatingActionButton fab = binding.fab;
      mTextView = binding.nest.longtext;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                mViewModel.getMessageContainerA().setValue("My name is Ram");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}