package com.example.scrollingactivity1;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.text.HtmlCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.scrollingactivity1.FileReadTest.EmployeeData;
import com.example.scrollingactivity1.FileReadTest.FileReadViewModel;
import com.example.scrollingactivity1.FileReadTest.FileRepository;
import com.example.scrollingactivity1.ui.main.TestViewModel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class ExcelFileReaderActivityTop extends AppCompatActivity     {





   private Button dummyButton;
    private Button dummyButton2;
    private Button dummyButton3;
    Context mContext ;

    int PICKFILE_RESULT_CODE = 1221;

    private TextView mTextView ;
    private TextView wrong_email;

    private FileReadViewModel  mViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  binding = ActivityExcelFileReaderBinding.inflate(getLayoutInflater());
        setContentView(R.layout.activity_excel_file_reader);
        mContext = this;

        mTextView = (TextView)findViewById(R.id.fullscreen_content);
        wrong_email = (TextView)findViewById(R.id.wrong_email);
        dummyButton = (Button)findViewById(R.id.dummy_button);
        dummyButton2 = (Button)findViewById(R.id.dummy_button2);


        dummyButton3 = (Button)findViewById(R.id.dummy_button3);
        dummyButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("*/*");
                chooseFile = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
            }
        });

        mViewModel = new ViewModelProvider(this).get(FileReadViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.getalldata().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!= null)
                    mTextView.setText(s);
                else mTextView.setText("String is null");
            }
        });

        mViewModel.getwrongdata().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!= null)
                    wrong_email.setText(s);
                else wrong_email.setText("String is null");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == PICKFILE_RESULT_CODE  && resultCode  == RESULT_OK) {

                Uri uri = data.getData();
                String src = uri.getPath();
                Toast.makeText(mContext, "Picked Path"+src+" ,file name : "+getFileName(uri),
                        Toast.LENGTH_SHORT).show();
                FileRepository.getInstance(mContext).getdataFromExcel(uri,mContext,mViewModel,this);
                //mTextView.setText(getdataFromExcel(uri));
                //File f = new File(uri);
               // show_wrong_email_data(uri);
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    public String getFileName(Uri returnUri)
    {
        Cursor returnCursor =
                getContentResolver().query(returnUri, null, null, null, null);

        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

        returnCursor.moveToFirst();
       String str = returnCursor.getString(nameIndex);

        return str;
    }





}