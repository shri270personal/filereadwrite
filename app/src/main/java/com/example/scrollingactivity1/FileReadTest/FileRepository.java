package com.example.scrollingactivity1.FileReadTest;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.text.HtmlCompat;

import com.example.scrollingactivity1.ui.main.ProjectRepository;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FileRepository {

    private static FileRepository mFileRepository;
    private int EXTERNAL_STORAGE_PERMISSION_CODE = 23;
    int PICKFILE_RESULT_CODE = 1221;
    List<EmployeeData> mEmployeeList;
    List<EmployeeData> mWrongEmail;
    Context mContext;

    private FileRepository()
    {

    }

    public  static  FileRepository getInstance( Context c)
    {

        if(mFileRepository==null)
        {
            mFileRepository = new FileRepository();
        }
        return  mFileRepository;
    }

    public void savePublicly(Activity activity , FileReadViewModel fileReadViewModel , String text) {
        // Requesting Permission to access External Storage
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        File file = new File(folder, "shriram.txt");
        writeTextData(file, text ,fileReadViewModel, activity);

    }

    private void writeTextData(File file, String data ,FileReadViewModel fileReadViewModel, Activity activity) {


        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
            Toast.makeText(activity, "Done" + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            fileReadViewModel.getalldata().setValue(data);
        } catch (Exception e) {

            fileReadViewModel.getalldata().setValue("Fail to Write Data");
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        showPublicData(fileReadViewModel);
    }

    public void showPublicData(FileReadViewModel fileReadViewModel) {
        // Accessing the saved data from the downloads folder
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        // geeksData represent the file data that is saved publicly
        File file = new File(folder, "shriram.txt");
        String data = getdata(file);
        if (data != null) {

            fileReadViewModel.getfileData().setValue(data);

           // mTextView.setText(data);
        } else {
            fileReadViewModel.getfileData().setValue("No Data Found");
           // mTextView.setText("No Data Found");
        }
    }



    private String getdata(File myfile) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(myfile);
            int i = -1;
            StringBuffer buffer = new StringBuffer();
            while ((i = fileInputStream.read()) != -1) {
                buffer.append((char) i);
            }
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public void show_wrong_email_data(Uri uri , FileReadViewModel fileReadViewModel , Context context ,Activity activity)
    {

        StringBuilder mWrongEmailData = new StringBuilder();


        mWrongEmail = new ArrayList<EmployeeData>();
        for(int i=1; i<mEmployeeList.size();i++)
        {

            Log.e("EMPLOYEE", "email- "+mEmployeeList.get(i).getEmail());
            if(!Patterns.EMAIL_ADDRESS.matcher(mEmployeeList.get(i).getEmail()).matches())
            {
                mWrongEmailData.append("[");
                mWrongEmailData.append(mEmployeeList.get(i).getRow_id()+","+mEmployeeList.get(i).getAdmission_no()+","+mEmployeeList.get(i).getFullname()+","+mEmployeeList.get(i).getEmail()+","+mEmployeeList.get(i).getCourse());

                mWrongEmail.add(mEmployeeList.get(i));
                mWrongEmailData.append("]");
            }

        }


fileReadViewModel.getwrongdata().setValue(mWrongEmailData.toString());

        writxlFile(mWrongEmail,uri, context, activity);
    }
    public void writxlFile(List<EmployeeData> list , Uri uri, Context context , Activity activity) {

        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(folder, "wrong_email_data.xlsx");
        FileOutputStream fileOutputStream = null;
        try {

            InputStream fileInputStream = context.getContentResolver().openInputStream(uri);


            XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = 0; i < list.size(); i++) {
                Row row = sheet.createRow(0);
                Cell cell1 = row.createCell(0);
                cell1.setCellValue(list.get(i).getRow_id()+10);

                Cell cell2 = row.createCell(0);
                cell2.setCellValue(list.get(i).getAdmission_no());

                Cell cell3 = row.createCell(0);
                cell1.setCellValue(list.get(i).getFullname());

                Cell cell4 = row.createCell(0);
                cell1.setCellValue(list.get(i).getEmail());

                Cell cell5 = row.createCell(0);
                cell1.setCellValue(list.get(i).getCourse());

            }
            fileOutputStream = new FileOutputStream(file);
            workbook.write(fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();


        } catch (Exception e) {
            Log.e("EXLFile", e.toString());
            e.printStackTrace();
        }

    }
    public void getdataFromExcel(Uri uri , Context cont , FileReadViewModel fileReadViewModel , Activity activity) {
        InputStream fileInputStream = null;
        mEmployeeList = new ArrayList<EmployeeData>();
        StringBuilder excel_data = new StringBuilder();

        try {
            //fileInputStream = new FileInputStream(myfile);
            fileInputStream = cont.getContentResolver().openInputStream(uri);
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);
                int rowsCount = sheet.getPhysicalNumberOfRows();
                FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
                for (int r = 0; r<rowsCount; r++) {
                    EmployeeData emp =  new EmployeeData();
                    Row row = sheet.getRow(r);
                    int cellsCount = row.getPhysicalNumberOfCells();
                    excel_data.append("[");
                    for (int c = 0; c<cellsCount; c++) {
                        String value = getCellAsString(row, c, formulaEvaluator, emp);
                        String cellInfo = ""+value+",";
                        excel_data.append(cellInfo);
                        //   excel_data.append("<br/>");

                    }
                    mEmployeeList.add(emp);
                    //   excel_data.append("<br/>");
                    excel_data.append("]");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("TAG", "error "+ e.toString());
        }
        fileReadViewModel.getalldata().setValue(excel_data.toString());
        show_wrong_email_data(uri,fileReadViewModel,cont , activity);
    }

    protected String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator , EmployeeData emp) {
        String value = "None";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);

            switch (c)
            {
                case 0:
                    emp.setRow_id((int)cellValue.getNumberValue());
                    break;
                case 1:
                    emp.setAdmission_no((int)cellValue.getNumberValue());
                    break;
                case 2:
                    emp.setFullname(cellValue.getStringValue());
                    break;
                case 3:
                    emp.setEmail(cellValue.getStringValue());
                    break;
                case 4:
                    emp.setCourse(cellValue.getStringValue());
                    break;
                default:
                    break;
            }
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();

                    value = ""+numericValue;

                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            /* proper error handling should be here */
            e.printStackTrace();
        }
        return value;
    }


}
