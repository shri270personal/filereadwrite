package com.example.scrollingactivity1.FileReadTest;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.jetbrains.annotations.NotNull;

public class FileReadViewModel extends ViewModel {


    private MutableLiveData<String> alldata = new MutableLiveData<String>();
    private MutableLiveData<String> wrongdata = new MutableLiveData<String>();
    private MutableLiveData<String> fileData = new MutableLiveData<String>();

    public  MutableLiveData<String> getalldata() {
        return alldata;
    }
    public  MutableLiveData<String> getwrongdata() {
        return wrongdata;
    }

    public  MutableLiveData<String> getfileData() {
        return fileData;
    }
/*
    public FileReadViewModel(Application mApplication , String text)
    {
        this(mApplication);
    }

 */
}
