package com.example.scrollingactivity1.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class TestViewModel extends ViewModel {
    private MutableLiveData<String> messageContainerA = new MutableLiveData<String>();
    private MutableLiveData<NewNameDetailList> newName = new MutableLiveData<NewNameDetailList>();
    private  LiveData<List<StoriesList>> getStoryList;
   // public LiveData<String> getMessageContainerA() {
   public  MutableLiveData<String> getMessageContainerA() {
        return messageContainerA;
    }

    public MutableLiveData<NewNameDetailList> getNewName()
    {
        return newName;
    }
    // TODO: Implement the ViewModel
}