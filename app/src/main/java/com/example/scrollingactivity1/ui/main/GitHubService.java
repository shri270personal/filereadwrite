package com.example.scrollingactivity1.ui.main;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GitHubService {

    public static  final String GET_BOOKING_TIPS = "http://exampractice.membrainsoft.com/services_live/getbookingtips.php";

    @GET("services_live/getbookingtips.php")
    Call<StoriesList> doGetStoriesList();


}
