package com.example.scrollingactivity1.ui.main;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectRepository {

    private static ProjectRepository mProjectRepository;

    private ProjectRepository()
    {

    }

    public  static  ProjectRepository getInstance()
    {
        if(mProjectRepository==null)
        {
            mProjectRepository = new ProjectRepository();
        }
        return  mProjectRepository;
    }


    public  void loadDataOnViewHolder(Context mContext , TestViewModel mTestViewHolder)
    {
        GitHubService mGithubService = ApiClient.getClient().create(GitHubService.class);
        Call<StoriesList> call=  mGithubService.doGetStoriesList();
        call.enqueue(new Callback<StoriesList>() {
            @Override
            public void onResponse(Call<StoriesList> call, Response<StoriesList> response) {
                StoriesList mList = response.body();

                List<StoryName> list = mList.getList();

                StringBuilder mStringb = new StringBuilder();
                for(int i =0 ; i<list.size();i++)
                {
                    if(list.get(i).getType().equals("items"))
                    {
                        mStringb.append(list.get(i).getSalesItemList().get(0).getItemName()+",");

                    }
                }

                mTestViewHolder.getMessageContainerA().setValue(mStringb.toString());
                Log.d("TAGRAM", "Number of items received: " + mStringb);
                Toast.makeText(mContext, mList.getResult(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<StoriesList> call, Throwable t) {
                Log.d("TAGRAMFAIL", "Number of movies received: " + t.toString());
                Toast.makeText(mContext,"FAIL", Toast.LENGTH_SHORT).show();
                mTestViewHolder.getMessageContainerA().setValue("Fail to Load Data");
            }
        });


    }


}
