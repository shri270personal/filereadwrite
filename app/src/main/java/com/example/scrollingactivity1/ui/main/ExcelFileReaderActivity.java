package com.example.scrollingactivity1.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.text.HtmlCompat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.scrollingactivity1.ui.main.databinding.ActivityExcelFileReaderBinding;
import com.example.scrollingactivity1.FileReadTest.EmployeeData;
import com.example.scrollingactivity1.R;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.TestOnly;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ExcelFileReaderActivity extends AppCompatActivity     {


    private final Handler mHideHandler = new Handler();
    private View mContentView;

    private View mControlsView;

    private boolean mVisible;

   private Button dummyButton;
    private Button dummyButton2;
    private Button dummyButton3;
Context mContext ;
    private int EXTERNAL_STORAGE_PERMISSION_CODE = 23;
    int PICKFILE_RESULT_CODE = 1221;
List<EmployeeData> mEmployeeList;
    List<EmployeeData> mWrongEmail;
    private TextView mTextView ;
    private TextView wrong_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  binding = ActivityExcelFileReaderBinding.inflate(getLayoutInflater());
        setContentView(R.layout.activity_excel_file_reader);
        mContext = this;
        mVisible = true;
        mTextView = (TextView)findViewById(R.id.fullscreen_content);
        wrong_email = (TextView)findViewById(R.id.wrong_email);


        dummyButton = (Button)findViewById(R.id.dummy_button);
        dummyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext , "button Click ", Toast.LENGTH_SHORT).show();
                savePublicly();
            }
        });

        dummyButton2 = (Button)findViewById(R.id.dummy_button2);
        dummyButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublicData();
            }
        });

        dummyButton3 = (Button)findViewById(R.id.dummy_button3);
        dummyButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("*/*");
                chooseFile = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == PICKFILE_RESULT_CODE  && resultCode  == RESULT_OK) {

                Uri uri = data.getData();
                String src = uri.getPath();
                Toast.makeText(mContext, "Picked Path"+src+" ,file name : "+getFileName(uri),
                        Toast.LENGTH_SHORT).show();
                Toast.makeText(mContext, "data in file "+getdataFromExcel(uri),
                        Toast.LENGTH_SHORT).show();
                mTextView.setText(getdataFromExcel(uri));
                //File f = new File(uri);
                show_wrong_email_data(uri);
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    public String getFileName(Uri returnUri)
    {
        Cursor returnCursor =
                getContentResolver().query(returnUri, null, null, null, null);

        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

        returnCursor.moveToFirst();
       String str = returnCursor.getString(nameIndex);

        return str;
    }

    public void savePublicly() {
        // Requesting Permission to access External Storage
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);
        String editTextData = "My name is ram";

        // getExternalStoragePublicDirectory() represents root of external storage, we are using DOWNLOADS
        // We can use following directories: MUSIC, PODCASTS, ALARMS, RINGTONES, NOTIFICATIONS, PICTURES, MOVIES
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        // Storing the data in file with name as geeksData.txt
        File file = new File(folder, "geeksData.txt");
        writeTextData(file, editTextData);

    }

    private void writeTextData(File file, String data) {


        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
            Toast.makeText(this, "Done" + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void showPublicData() {
        // Accessing the saved data from the downloads folder
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        // geeksData represent the file data that is saved publicly
        File file = new File(folder, "geeksData.txt");
        String data = getdata(file);
        if (data != null) {
            mTextView.setText(data);
        } else {
            mTextView.setText("No Data Found");
        }
    }

    private String getdataFromExcel(Uri uri) {
         InputStream fileInputStream = null;
mEmployeeList = new ArrayList<EmployeeData>();
        StringBuilder excel_data = new StringBuilder();

        try {
            //fileInputStream = new FileInputStream(myfile);
            fileInputStream = getContentResolver().openInputStream(uri);
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);
                int rowsCount = sheet.getPhysicalNumberOfRows();
                FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
                for (int r = 0; r<rowsCount; r++) {
                    EmployeeData emp =  new EmployeeData();
                    Row row = sheet.getRow(r);
                    int cellsCount = row.getPhysicalNumberOfCells();
                    excel_data.append("[");
                    for (int c = 0; c<cellsCount; c++) {
                        String value = getCellAsString(row, c, formulaEvaluator, emp);
                        String cellInfo = ""+value+",";
                        excel_data.append(cellInfo);
                       //   excel_data.append("<br/>");

                    }
                    mEmployeeList.add(emp);
                 //   excel_data.append("<br/>");
                    excel_data.append("]");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("TAG", "error "+ e.toString());
        }

        return excel_data.toString();
    }



    public void show_wrong_email_data(Uri uri)
    {

     StringBuilder mWrongEmailData = new StringBuilder();


        mWrongEmail = new ArrayList<EmployeeData>();
        for(int i=1; i<mEmployeeList.size();i++)
        {

            Log.e("EMPLOYEE", "email- "+mEmployeeList.get(i).getEmail());
                 if(!Patterns.EMAIL_ADDRESS.matcher(mEmployeeList.get(i).getEmail()).matches())
            {
                mWrongEmailData.append("[");
                mWrongEmailData.append(mEmployeeList.get(i).getRow_id()+","+mEmployeeList.get(i).getAdmission_no()+","+mEmployeeList.get(i).getFullname()+","+mEmployeeList.get(i).getEmail()+","+mEmployeeList.get(i).getCourse());

                mWrongEmail.add(mEmployeeList.get(i));
                mWrongEmailData.append("]");
            }

        }



        wrong_email.setText(HtmlCompat.fromHtml(mWrongEmailData.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY));

        writxlFile(mWrongEmail,uri);
    }



    protected String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator , EmployeeData emp) {
        String value = "None";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);

            switch (c)
            {
                case 0:
                    emp.setRow_id((int)cellValue.getNumberValue());
                    break;
                case 1:
                    emp.setAdmission_no((int)cellValue.getNumberValue());
                    break;
                case 2:
                    emp.setFullname(cellValue.getStringValue());
                    break;
                case 3:
                    emp.setEmail(cellValue.getStringValue());
                    break;
                case 4:
                    emp.setCourse(cellValue.getStringValue());
                    break;
                default:
                    break;
            }
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();

                        value = ""+numericValue;

                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            /* proper error handling should be here */
            e.printStackTrace();
        }
        return value;
    }

    public void writxlFile(List<EmployeeData> list , Uri uri) {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(folder, "wrong_email_data.xlsx");
        FileOutputStream fileOutputStream = null;
        try {

            InputStream fileInputStream = getContentResolver().openInputStream(uri);


                XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = 0; i < list.size(); i++) {
                Row row = sheet.createRow(0);
                Cell cell1 = row.createCell(0);
                cell1.setCellValue(list.get(i).getRow_id()+10);

                Cell cell2 = row.createCell(0);
                cell2.setCellValue(list.get(i).getAdmission_no());

                Cell cell3 = row.createCell(0);
                cell1.setCellValue(list.get(i).getFullname());

                Cell cell4 = row.createCell(0);
                cell1.setCellValue(list.get(i).getEmail());

                Cell cell5 = row.createCell(0);
                cell1.setCellValue(list.get(i).getCourse());

            }
            fileOutputStream = new FileOutputStream(file);
            workbook.write(fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

            //fileOutputStream.close();
           /*
         //   InputStream stream = getResources().openRawResource(R.raw.template);
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(stream);
                XSSFSheet sheet = workbook.getSheetAt(0);
                //XSSFWorkbook workbook = new XSSFWorkbook();
                //XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("mysheet"));
                for (int i = 0; i < 10; i++) {
                    Row row = sheet.createRow(i);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(i);
                }
                String outFileName = "filetoshare.xlsx";
                //   printlnToUser("writing file " + outFileName);
                File cacheDir = getCacheDir();
                File outFile = new File(cacheDir, outFileName);
                OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath());
                workbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
                // printlnToUser("sharing file...");

            } catch (Exception e) {

                e.printStackTrace();
            }

             */
        } catch (Exception e) {
            Log.e("EXLFile", e.toString());
            e.printStackTrace();
        }

    }
    private String getdata(File myfile) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(myfile);
            int i = -1;
            StringBuffer buffer = new StringBuffer();
            while ((i = fileInputStream.read()) != -1) {
                buffer.append((char) i);
            }
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}